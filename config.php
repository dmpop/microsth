<?php
    // microsth base directory
    $base_dir = 'microsth';
    // Title
    $title = 'micro.sth';
    // Default category
    $first_page = 'Home';
    //Enable password protection
    $protect = true;
    // Password
    $passwd = 'password';
    // Enable New Page button
    $newpage = true;
    // Enable Trash button
    $trash = true;
    // Footer
    $footer = "<a href='https://gitlab.com/dmpop/micro.sth'>micro.sth</a> - write here";
    // Resize image size
    $resize = '800';
?>
